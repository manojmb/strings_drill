// Function ipAddressSplit split the ip address and stores it in an array
function ipAddressSplit(address) {
    // The split function will split the address depending on parameter given
    address = address.split(".");
    // Converts string to int in array
    address = address.map((num) => {
        return Number(num);
    })
    // Returns array of address separated
    return address;
}
// Exports function ipAddressSplit which can be used in other files using import
module.exports = { ipAddressSplit };