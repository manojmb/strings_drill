// Function rrayJoinNames accepts array and joins the array items
function arrayJoinNames(names) {
    if (Array.isArray(names)) {
        // Checks whether array length greater than 0
        if (names.length > 0) {
            names = names.join(' ');
            return names;
        }
        // Returns empty array if given array is empty
        return "";
    }
    // Returns if array is not input
    return "Provide array as input";
}
// Exports function arrayJoinNames which can be used in other files
module.exports = { arrayJoinNames };