// Function titleFullName accepts object and return the full name in title case.

function titleFullName(personName) {
    if (typeof personName === 'object') {
        // fullName to hold complete name of person
        let fullName = "";
        // Checking if object property exists
        if (typeof personName.first_name != "undefined") {
            // Converting first letter to uppercase and rest to lower case
            fullName += `${personName.first_name.slice(0, 1).toUpperCase()}${personName.first_name.slice(1).toLowerCase()} `;
        }
        if (typeof personName.middle_name != "undefined") {
            fullName += `${personName.middle_name.slice(0, 1).toUpperCase()}${personName.middle_name.slice(1).toLowerCase()} `;
        }
        if (typeof personName.last_name != "undefined") {
            fullName += `${personName.last_name.slice(0, 1).toUpperCase()}${personName.last_name.slice(1).toLowerCase()}`;
        }
        return fullName;
    }
    // If input is not object
    return "Provide proper input";
}

// Exports function titleFullName which can be used in other files
module.exports = { titleFullName };