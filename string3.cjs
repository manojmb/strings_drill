// Function dateOfMonth gives us month in which date is present
function dateOfMonth(string) {
    // Splits string based on / and converts to array
    string = string.split('/');
    // month array
    const month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    // parseInt converts to integer 
    // Returns month of date
    return month[parseInt(string[1]) - 1];
}

// Exports function dateOfMonth which can be used in other files using import
module.exports = { dateOfMonth };