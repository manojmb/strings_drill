// Function convertToInt  converts given strings into their equivalent numeric format without any precision loss 
function convertToInt(string) {
    // Takes any input and converts into string
    string = String(string);
    // nums is variable which holds values present in proper number
    const nums = "0123456789+-.";
    // numberString empty string which will hold number when iterated through string
    let numberString = "";
    // Iterates through string string
    for (let index = 0; index < string.length; index++) {
        // Checks if char at particular index is included in nums
        if (nums.includes(string.charAt(index))) {
            // Appending number to variable numberString
            numberString += string.charAt(index);
        }
    }
    // Checks whether numberString is number or not
    if (isNaN(Number(numberString))) {
        // Returns 0 if not a number
        return 0;
    }
    // Returns number string
    return numberString;
}
// Exports function convertToInt which can be used in other files using import
module.exports = { convertToInt };
