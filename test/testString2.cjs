// Imports function ipAddressSplit from string2.cjs
const { ipAddressSplit } = require("../string2.cjs");

// The try block executes function but if any error occur it will be catched by catch block
try {
    const ip = "111.139.161.143";
    // The result variable willhave result by executing ipAddressSplit function to check whether number or not
    result = ipAddressSplit(ip);
    console.log("\nThe result of test string 2:-");
    // Logs result in console window
    console.log(result);
} catch (e) {
    // Log error message if encountered any
    console.log(e.message);
}