// Imports function titleFullName from string4.cjs
const { titleFullName } = require("../string4.cjs");

// The try block executes function but if any error occur it will be catched by catch block
try {
    personName = { "first_name": "JoHN", "last_name": "SMith" };
    console.log("\nThe result of test string 4:-");
    result = titleFullName(personName);
    console.log(result);
    personName = { "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" };
    result = titleFullName(personName);
    console.log(result);

} catch (e) {
    // Log error message if encountered any
    console.log(e.message);
} 