// Imports function titleFullName from string4.cjs
const { arrayJoinNames } = require("../string5.cjs");

// The try block executes function but if any error occur it will be catched by catch block
try {
    namesArray = ["the", "quick", "brown", "fox"];
    result = arrayJoinNames(namesArray);
    console.log("\nThe result of test string 5:-");
    console.log(result);
} catch (e) {
    // Log error message if encountered any
    console.log(e.message);
} 