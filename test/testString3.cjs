// Imports function dateOfMonth from string3.cjs
const { dateOfMonth } = require("../string3.cjs");

// The try block executes function but if any error occur it will be catched by catch block
try {
    const date = "20/1/2021";
    // The result variable will have result by executing dateOfMonth function to get month
    result = dateOfMonth(date);
    console.log("\nThe result of test string 3:-");
    // Logs result in console window
    console.log(result);
} catch (e) {
    // Log error message if encountered any
    console.log(e.message);
}