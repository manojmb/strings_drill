// Imports function ipAddressSplit from string1.cjs
const { convertToInt } = require("../string1.cjs");

// The try block executes function but if any error occur it will be catched by catch block
try {
    // The result variable will execute convertToInt function to check whether number or not
    result = convertToInt("$124.45");
    console.log("The result of test string 1:-");
    // Logs result in console window
    console.log(result);
    result = convertToInt("$1,002.22");
    console.log(result);
    result = convertToInt("$67..42.5");
    console.log(result);
    result = convertToInt(44.4);
    console.log(result);
} catch (e) {
    // Log error message if encountered any
    console.log(e.message);
}